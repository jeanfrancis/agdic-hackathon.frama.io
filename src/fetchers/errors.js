import OError from "@overleaf/o-error"

export class ConfigError extends OError {
  constructor(options) {
    super({ message: "Config error", ...options })
  }
}

export class FetchError extends OError {
  constructor(options) {
    super({ message: "Fetch error", ...options })
  }
}