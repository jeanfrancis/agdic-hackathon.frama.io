
// From https://github.com/matyunya/smelte/blob/master/postcss.config.js
module.exports = (purge = false) => {
  return [
    require("postcss-import")(),
    require("autoprefixer")(),
    require("tailwindcss")("./tailwind.config.js"),
    purge &&
    require("cssnano")({
      preset: "default"
    }),
    purge &&
    require("@fullhuman/postcss-purgecss")({
      content: ["./**/*.svelte"],
      extractors: [
        {
          extractor: content => {
            const fromClasses = content.match(/class:[A-Za-z0-9-_]+/g) || []

            return [
              ...(content.match(/[A-Za-z0-9-_:\/]+/g) || []),
              ...fromClasses.map(c => c.replace("class:", ""))
            ]
          },
          extensions: ["svelte"]
        }
      ],
      whitelist: [
        "html", "body", "stroke-primary",
        // for JS ripple
        "bg-white-trans",
        // for AppBar buttons
        "border-black"
      ],
      whitelistPatterns: [
        // for JS ripple
        /ripple/
      ]
    })
  ].filter(Boolean)
}